puts "I am thinking of a number."
puts "Can you guess it?"

randomNumber = rand(100)
puts randomNumber

counter = 0

loop do
  guess = gets.to_i
  puts "Too high" if guess > randomNumber
  puts "Too low" if guess < randomNumber
  counter = counter + 1
  break if guess == randomNumber
end

puts "It took you #{counter} guesses"
